SELECT
  folder,
  MAX(to_folder) AS to_folder,
  MAX(Migrated) AS Migrated,
  MAX(Waiting) AS Waiting,
  MAX(Errored) AS Errored
FROM (
SELECT folder, IFNULL(migrate_folder, '') AS to_folder, 0 AS Waiting, COUNT(uid) AS Migrated, 0 AS Errored FROM message WHERE migrated IS NOT NULL GROUP BY folder, migrate_folder
UNION ALL
SELECT folder, IFNULL(migrate_folder, '') AS to_folder, COUNT(uid) AS Waiting, 0 AS Migrated, 0 AS Errored FROM message WHERE migrated IS NULL GROUP BY folder, migrate_folder
UNION ALL
SELECT folder, IFNULL(migrate_folder, '') AS to_folder, 0 AS Waiting, 0 AS Migrated, COUNT(uid) AS Errored FROM message WHERE migrated IS NULL AND migration_attempts > 2 GROUP BY folder, migrate_folder)
GROUP BY folder