SELECT
  host,
  COUNT(uidvalidity) AS FOLDER_ID,
  COUNT(folder) AS FOLDERS,
  uid
FROM message
GROUP BY
  host, uid
HAVING
  COUNT(folder) >= 2