import os
import sys
from uuid import uuid4


class Logger(object):

    logger = None
    DISPLAY = 0
    DETAIL = 1
    DEBUG = 2
    TRACE = 3

    def __init__(self):
        self._output = []
        self._indent = 0
        self._level = 3
        self._last_msg_length = 0
        self.flush_level = 100

    @property
    def level(self):
        return self._level

    def set_level(self, value):
        self._level = value

    def indent(self):
        self._indent += 1

    def dedent(self):
        self._indent -= 1

    def dedent_all(self):
        self._index = 0

    def new_line(self):
        sys.stderr.write('\n')

    def save(self, output='log.txt'):
        with open(output, 'a') as file_:
            file_.write('\n'.join(self._output))
        self._output = []

    def __call__(self, msg, *args, **kwargs):
        if not self.level >= kwargs.get('level', 3):
            return

        end_of_line = '\n'

        if not isinstance(msg, basestring):
            msg = str(msg)

        msg = ('    ' * self._indent) + msg.format(*args)
        current_msg_length = len(msg)

        if 'overwrite' in kwargs:
            # remove carriage return
            end_of_line = ''
            # make sure we write over last line length
            msg = '\r' + msg.ljust(self._last_msg_length, " ") + '\r'

        sys.stderr.write(msg)
        sys.stderr.write(end_of_line)

        self._last_msg_length = current_msg_length

        if 'overwrite' not in kwargs:
            self._output.append(msg)

        if len(self._output) > self.flush_level:
            self.save()

    # 0 display 
    # 1 detail
    # 2 debug
    # 3 trace

    def display(self, msg, *args, **kwargs):
        kwargs.update(dict(level=0))
        self(msg, *args, **kwargs)

    def detail(self, msg, *args, **kwargs):
        kwargs.update(dict(level=1))
        self(msg, *args, **kwargs)

    def debug(self, msg, *args, **kwargs):
        kwargs.update(dict(level=2))
        self(msg, *args, **kwargs)

    def trace(self, msg, *args, **kwargs):
        kwargs.update(dict(level=3))
        self(msg, *args, **kwargs)

    def overwrite(self, msg, *args):
        self(msg, *args, level=0, overwrite=True)

    def line(self, length=60, level=0):
        self('-'*length, level=level)

    @staticmethod
    def get():
        if Logger.logger is None:
            Logger.logger = Logger()
        return Logger.logger

    @staticmethod
    def log(msg, *args, **kwargs):
        level = kwargs.get('level', 3)
        logger = Logger.get()
        logger(msg, *args, level=level)


logger = Logger.get()


def chunks(iterable, chunk):
    "Yield successive n-sized chunks from iterable."
    for i in xrange(0, len(iterable), chunk):
        yield iterable[i:i+chunk]

def uuid_gen():
    return uuid4().hex.upper()

def save_to_local(directory, uuid, message):
    path = os.path.join(directory, uuid + '.txt') 
    with open(path, 'w') as fh_:
        fh_.write(message)

def normalize_folders(folder_name, separator='.'):
    return os.path.join(*folder_name.split(separator))

