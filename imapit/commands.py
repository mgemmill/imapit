import sys
import getpass
import socket
import time
from getpass import getpass
from functools import wraps
import traceback
from imaplib import IMAP4
from imapclient import IMAPClient
from util import logger as log
from util import chunks
import db
import op


FOLDER_TRANSLATION_INTRO = (
"# \n"
"# \n"
"# This is a listing of imap folders on your server.\n"
"# If you want to rename these folders on the account you are transferring to,\n"
"# then input the new name after the '-- ## -->' on the same line.\n"
"# \n"
"# For example, leaving the following line as is will transfer this folder name as is:\n"
"# \n"
"#     [Gmail]/All Mail -- 23456 -->\n"
"# \n"
"# However, this will place all emails in the 'Inbox' folder on the new account:\n"
"# \n"
"#     [Gmail]/All Mail -- 23456 --> Inbox\n"
"# \n"
"# Pass this file to the copy command's `--translation` argument will apply these\n"
"# translations.\n"
"# \n"
"# \n\n"
)


def parse_input(input_func, query, validate=lambda input: input != ''):
    valid_input = False 
    input_value = ''
    counter = 0
    while valid_input is not True:
        if counter:
            log.display('unacceptable value, please try again...')
        input_value = input_func(query).strip()
        valid_input = validate(input_value)
    return input_value



def create_server(host, user, password, use_ssl=True):
    def _create_instance():
        server = IMAPClient(host, use_uid=True, ssl=use_ssl)
        server.login(user, password)
        return server
    return _create_instance


class login(object):
    '''
    Decorator to handle imap account login.

    '''
    def __init__(self, keyword='account', host_arg='host', user_arg='user', pass_arg='password'):
        self.keyword = keyword
        self.account = None
        self.user = ''
        self.password = ''
        self.host_arg = host_arg
        self.user_arg = user_arg
        self.pass_arg = pass_arg

    def handle_login(self, args):

        def parse_input(input_func, query):
            bogus_input = True
            input_value = ''
            counter = 0
            while bogus_input:
                if counter:
                    log.display('incorrect value, please try again...')
                input_value = input_func(query).strip()
                bogus_input = (input_value == '')
            return input_value

        self.host = getattr(args, self.host_arg, '')
        self.user = getattr(args, self.user_arg, '')
        self.password = getattr(args, self.pass_arg, '')

        if self.host == '':
            self.host = parse_input(raw_input, 'host: ')
        if self.user == '':
            self.user = parse_input(raw_input, 'user: ')
        if self.password == '':
            self.password = parse_input(getpass.getpass, 'password: ')

        log.debug('ACCOUNT: {0}', self.keyword)
        log.debug('{0}: {1}', self.host_arg, self.host)
        log.debug('{0}: {1}', self.user_arg, self.user)
        log.debug('{0}: {1}', self.pass_arg, '*'*(len(self.password)))


    def __call__(self, func):
        @wraps(func)
        def decorator(args, **kwargs):
            self.handle_login(args)
            self.server = create_server(self.host, self.user, self.password)
            kwargs.update({ self.keyword: self.server })
            return func(args, **kwargs)
        return decorator
        
        

@login(keyword='SERVER')
def mailbox(args, SERVER=None):
    server = SERVER()
    
    mailboxes = op.fetch_folder_status(server)
    
    log.display('{0} Account Folders:', args.host)
    log.indent()

    with open(args.output, 'w') as fh_:
        fh_.write(FOLDER_TRANSLATION_INTRO)
        for mb in mailboxes:
            log.display('{0} (MESSAGES: {1})'.format(mb.name, mb.msg_count))
            fh_.write('{0} -- {1} --> \n'.format(mb.name, mb.msg_count))

    log.dedent()


def log_error():
    exc_type, exc_value, exc_traceback = sys.exc_info()
    err = traceback.format_exception(exc_type, exc_value, exc_traceback)
    log.display(err[-1])
    log.debug('-'*60)
    log.debug(''.join(err))
    log.debug('-'*60)
    log.save()


def pause(for_delay):
    log.display('connection error - pausing for restart....')
    time.sleep(for_delay)


@login('FROM')
def meta(args, FROM=None):

    imap_delay, batch_delay, batch_size, err_delay= args.throttle

    log.display('copying from imap account {0} info to database', args.host)

    FETCHING_MAIL = True

    while FETCHING_MAIL:
        try:
            op.record_account_meta_data(args.host, FROM, args.database, args.exclude, args.include) 
            FETCHING_MAIL = False
        except KeyboardInterrupt as ex:
            raise ex
        except (IMAP4.error, socket.error) as ex:
            log_error()
            pause(err_delay)
        except Exception as ex:
            log_error()
            raise ex


@login(keyword='FROM')
@login('TO', 'to_host', 'to_user', 'to_password')
def copy(args, FROM=None, TO=None):

    imap_delay, batch_delay, batch_size, err_delay= args.throttle

    log.display('copying from imap account {0} to {1}', args.host, args.to_host)

    FETCHING_MAIL = False if args.skip_meta else True

    while FETCHING_MAIL:
        try:
            op.record_account_meta_data(args.host, FROM, args.database, args.exclude, args.include) 
            FETCHING_MAIL = False
        except KeyboardInterrupt as ex:
            raise ex
        except (IMAP4.error, socket.error) as ex:
            log_error()
            pause(err_delay)
        except Exception as ex:
            log_error()
            raise ex

    MIGRATING_MAIL = True
            
    log.display('moving from imap account {0} to {1}', args.host, args.to_host)
    while MIGRATING_MAIL:
        try:
            op.migrate_mail(args.host, 
                            FROM, 
                            args.to_host, 
                            TO, 
                            args.database, 
                            args.translation, 
                            imap_delay, 
                            batch_delay, 
                            batch_size) 

            MIGRATING_MAIL = False
        except KeyboardInterrupt as ex:
            raise ex
        except (IMAP4.error, socket.error) as ex:
            log_error()
            pause(err_delay)
        except Exception as ex:
            log_error()
            raise ex


@login('FROM')
def backup(args, FROM=None):

    imap_delay, batch_delay, batch_size, err_delay= args.throttle

    log.display('copying from imap account {0} to {1}', args.host, args.local_dir)

    FETCHING_MAIL = False if args.skip_meta else True

    while FETCHING_MAIL:
        try:
            op.record_account_meta_data(args.host, FROM, args.database, args.exclude, args.include) 
            FETCHING_MAIL = False
        except KeyboardInterrupt as ex:
            raise ex
        except (IMAP4.error, socket.error) as ex:
            log_error()
            pause(err_delay)
        except Exception as ex:
            log_error()
            raise ex

    BACKUP_MAIL = True
            
    log.display('backup from imap account {0} to {1}', args.host, args.local_dir)
    while BACKUP_MAIL:
        try:
            op.backup_mail(args.host, 
                           FROM, 
                           args.database, 
                           args.translation, 
                           args.local_dir,
                           imap_delay, 
                           batch_delay, 
                           batch_size) 

            BACKUP_MAIL = False
        except KeyboardInterrupt as ex:
            raise ex
        except (IMAP4.error, socket.error) as ex:
            log_error()
            pause(err_delay)
        except Exception as ex:
            log_error()
            raise ex
