import sys
import args
from util import logger as log


def main():
    argv = None
    try:
        argv = args.parse()
        log.set_level(argv.verbose)
        log.line()
        argv.func(argv)
        log.save()
        sys.exit(0)
    except KeyboardInterrupt:
        log.display('imapit cancelled with ctrl-C')
        log.save()
        sys.exit(0)
    except Exception as ex:
        log.dedent_all()
        log.display('An error occurred with imapit:')
        if argv.verbose >= log.DEBUG:
            import traceback
            log.line()
            traceback.print_exc(limit=9, file=sys.stdout)
            log.line()
        else:
            log.display(ex)
        log.save()
        sys.exit(1)


if __name__ == '__main__':
    main()

