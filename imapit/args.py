'''

basic usage:


mailbox

To list all mailboxes:

... imapit mailbox --host imap.server.com --user username

To print status info on all mailboxes:

... imapit mailbox --host imap.server.com --user username --status

To print the first 25 email ids within each mailbox:

... imapit mailbox --host imap.server.com --user username --show-message-ids

To print more email ids within each mailbox:

... imapit mailbox --host imap.server.com --user username --show-message-ids --limit 50

email

To show a particular email's info

... imapit email --host imap.server.com --user username mailboxname 3

To show a particular email including the body

... imapit email --host imap.server.com --user username --body mailboxname 3

'''
import argparse
import commands

__version__ = '0.2.6'

def standard_args(parser):
    parser.add_argument('--host', '-H', type=str, default='')
    parser.add_argument('--user', '-U', type=str, default='')
    parser.add_argument('--password', '-P', type=str, default='')
    parser.add_argument('--verbose', '-v', type=int, choices=[0,1,2,3], default=0)


def throttle(value):
    '''
    Throttle arg format: 1:2:3:4
    1. sleep x seconds between each email
    2. sleep x seconds between batches of email
    3. batch email size
    4. sleep x seconds between connection errors
    '''
    msg = 'throttle argument format is 999:999:999:999'
    args = value.split(':')
    if len(args) != 4:
        raise argparse.ArgumentTypeError(msg)
    try:
        return tuple(int(v) for v in args)
    except:
        raise argparse.ArgumentTypeError(msg)


#>>> def perfect_square(string):
#...     value = int(string)
#...     sqrt = math.sqrt(value)
#...     if sqrt != int(sqrt):
#...         msg = "%r is not a perfect square" % string
#...         raise argparse.ArgumentTypeError(msg)
#...     return valueVk

def parse():
    parser = argparse.ArgumentParser(prog='imapit')
    parser.add_argument('--version', '-V', action='version', version='%(prog)s v{0}'.format(__version__)) 
    subparser = parser.add_subparsers()

    # mailbox
    mailbox_cmd_args = subparser.add_parser('mailbox')
    standard_args(mailbox_cmd_args)
    mailbox_cmd_args.add_argument('--output', type=str, default='imapit_folder_map.txt')
    mailbox_cmd_args.set_defaults(func=commands.mailbox)

    # meta 
    meta_cmd_args = subparser.add_parser('meta')
    standard_args(meta_cmd_args)
    meta_cmd_args.add_argument('--exclude', nargs='*', default='')
    meta_cmd_args.add_argument('--include', nargs='*', default='')
    meta_cmd_args.add_argument('--database', type=str, default='imapit.db')
    meta_cmd_args.add_argument('--throttle', type=throttle, default='0:60:100:30')
    meta_cmd_args.set_defaults(func=commands.meta)

    # copy
    copy_cmd_args = subparser.add_parser('copy')
    standard_args(copy_cmd_args)
    copy_cmd_args.add_argument('--to-host', type=str, default='')
    copy_cmd_args.add_argument('--to-user', type=str, default='')
    copy_cmd_args.add_argument('--to-password', type=str, default='')
    copy_cmd_args.add_argument('--exclude', nargs='*', default='')
    copy_cmd_args.add_argument('--include', nargs='*', default='')
    copy_cmd_args.add_argument('--translation', type=str, default='imapit.trans')
    copy_cmd_args.add_argument('--database', type=str, default='imapit.db')
    copy_cmd_args.add_argument('--skip-meta', action='store_true', default=False)
    copy_cmd_args.add_argument('--throttle', type=throttle, default='0:60:100:30')
    copy_cmd_args.set_defaults(func=commands.copy)

    # backup
    backup_cmd_args = subparser.add_parser('backup')
    standard_args(backup_cmd_args)
    backup_cmd_args.add_argument('--exclude', nargs='*', default='')
    backup_cmd_args.add_argument('--include', nargs='*', default='')
    backup_cmd_args.add_argument('--translation', type=str, default='imapit.trans')
    backup_cmd_args.add_argument('--local-dir', type=str, default='.')
    backup_cmd_args.add_argument('--database', type=str, default='imapit.db')
    backup_cmd_args.add_argument('--skip-meta', action='store_true', default=False)
    backup_cmd_args.add_argument('--throttle', type=throttle, default='0:60:100:30')
    backup_cmd_args.set_defaults(func=commands.backup)

    return parser.parse_args()
