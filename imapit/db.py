import os
from datetime import datetime
from sqlite3 import connect 
from sqlite3 import IntegrityError
from collections import namedtuple
from util import logger as log


DEFAULT_NAME = 'imapit.db'


class Select(object):

    def __init__(self, sql, mapper=None):
        self.sql = sql
        if mapper:
            self.mapper = namedtuple('RowMap', mapper)
        else:
            self.mapper = mapper

    def __call__(self, db, *args):
        rows = db.execute(self.sql.format(*args))
        if self.mapper:
            return [self.mapper(*row) for row in rows]


class Update(object):

    def __init__(self, sql):
        self.sql = sql

    def __call__(self, db, *args):
        sql = self.sql.format(*args)
        log.trace(sql)
        db.execute(sql)
        db.commit()
        return db


create = Update((
    "CREATE TABLE message ("
    "host TEXT,"
    "uid INTEGER,"
    "uidvalidity TEXT,"
    "folder TEXT,"
    "delimiter TEXT,"
    "internaldate TEXT,"
    "flags TEXT,"
    "migrate_host TEXT,"
    "migrate_uid INTEGER,"
    "migrate_folder TEXT,"
    "migrated TEXT,"
    "migration_attempts INTEGER DEFAULT 0,"
    "local_id TEXT,"
    "local_folder TEXT,"
    "local_attempts INTEGER DEFAULT 0,"
    "created TEXT DEFAULT CURRENT_TIMESTAMP,"
    "edited TEXT DEFAULT CURRENT_TIMESTAMP,"
    "PRIMARY KEY (folder, uidvalidity, uid) ON CONFLICT ROLLBACK,"
    "UNIQUE(migrate_uid, migrate_folder) ON CONFLICT ROLLBACK"
    ")"))


def connection(database=DEFAULT_NAME):
    return connect(database)


def exists(database=DEFAULT_NAME):
    return os.path.exists(database)


def initialize(database=DEFAULT_NAME):
    if not exists(database):
        return create(connection(database))
    return connection(database)


insert = Update((
"INSERT INTO message (uid, host, folder, uidvalidity, delimiter, internaldate, flags) VALUES ("
"'{}','{}','{}','{}','{}', '{}', '{}')"))


update = Update((
    "UPDATE message SET "
    "migrate_host='{}',"
    "migrate_uid={},"
    "migrate_folder='{}',"
    "migrated=CURRENT_TIMESTAMP "
    "WHERE "
    "folder='{}' AND "
    "uidvalidity='{}' AND "
    "uid={}"))


update_attempt = Update((
    "UPDATE message SET "
    "migration_attempts=migration_attempts + 1 "
    "WHERE "
    "folder='{}' AND "
    "uidvalidity='{}' AND "
    "uid={}"))


update_local = Update((
    "UPDATE message SET "
    "local_id='{}',"
    "local_folder='{}' "
    "WHERE "
    "folder='{}' AND "
    "uidvalidity='{}' AND "
    "uid={}"))


update_local_attempt = Update((
    "UPDATE message SET "
    "local_attempts=local_attempts + 1 "
    "WHERE "
    "folder='{}' AND "
    "uidvalidity='{}' AND "
    "uid={}"))


class IMAPFolderInfo(object):

    def __init__(self, row):
        self.name = row[0]
        self.uidvalidity = row[1]
        self.msg_count = row[2]

    @property
    def migration_name(self):
        return getattr(self, '_new_folder', self.name)

    @migration_name.setter
    def migration_name(self, value):
        self._new_folder = value


SUMMARY = "SELECT folder, uidvalidity, COUNT(uid) AS messages FROM message WHERE host = '{}' GROUP BY host, folder, uidvalidity"

def folder_info(db, host):
    return dict([(row[0], IMAPFolderInfo(row)) for row in db.execute(SUMMARY.format(host))])


def summary(db, host):
    folder_counts = {}
    for row in db.execute(SUMMARY.format(host)):
        folder_counts[row[0]] = row[2]
    return folder_counts


def folders(db, host):
    # return a folder name map where the key is the same as the value
    folder_map = {}
    for row in db.execute(SUMMARY.format(host)):
        folder_map[row[0]] = row[0]
    return folder_map


MESSAGE_IDS = "SELECT uid FROM message WHERE folder = '{0}'"

def message_ids(db, folder):
    return [row[0] for row in db.execute(MESSAGE_IDS.format(folder))]


class MigrateMessage(namedtuple('MigrateMessage', 'uid folder internaldate flags')):

    def parse_flags(self):
        flags = self.flags.split(' ')
        if not flags:
            return 
        return flags

    def parse_internaldate(self):
        try:
            return datetime.strptime('%Y-%m-%d %H:%M:%S', self.internaldate)
        except:
            return datetime.now()


MIGRATION_MESSAGES = (
"SELECT uid, folder, internaldate, flags "
"FROM message WHERE folder = '{0}' AND "
"migrated IS NULL AND "
"migration_attempts < 3")

def migration_messages(db, folder):
    rows = db.execute(MIGRATION_MESSAGES.format(folder))
    return [MigrateMessage(uid=row[0], folder=row[1], internaldate=row[2], flags=row[3]) for row in rows]


backup_messages = Select((
    "SELECT uid, folder, internaldate, flags "
    "FROM message WHERE folder = '{0}' AND "
    "local_id IS NULL AND "
    "local_attempts < 3"), 
    'uid folder internaldate flags')



if __name__ == '__main__':
    db_file = 'test.db'
    if exists(db_file):
        os.remove(db_file)
    conn = initialize(db_file) 
    insert(conn, '123456', 'imap.gmail.com', 'Deleted', '5-Jul-2015 05:45:00 +0700', '(\Seen)')
    update(conn, '123456', 'mail.webfaction.com', '567897', 'Deleted Items')
