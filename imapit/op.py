import os
import re
import time
import shutil
import traceback
from collections import namedtuple
from imaplib import IMAP4
import db
import util
from util import logger as log
from util import chunks


class Mailbox(namedtuple('Mailbox', 'name flags delimiter uidvalidity msg_count')):
    pass


def fetch_folder_status(server):
    mailboxes = []
    _mb = server.list_folders()
    
    for mb_flags, mb_delimiter, mb_name in _mb:
        mb_status = None
        if server.folder_exists(mb_name) and '\\Noselect' not in mb_flags:
            mb_status = server.folder_status(mb_name)
        mb_status = mb_status if isinstance(mb_status, dict) else {}
        mailboxes.append(Mailbox(name=mb_name,
                                 flags=mb_flags,
                                 delimiter=mb_delimiter,
                                 uidvalidity=mb_status.get('UIDVALIDITY', 0),
                                 msg_count=mb_status.get('MESSAGES', 0)
                                 ))
    return mailboxes


def record_account_meta_data(imap_host, server_factory, database_file, exclude, include): 

    log.display('Recording imap account {0} meta data', imap_host)

    server= server_factory()
    mailboxes = fetch_folder_status(server)

    if not exclude:
        exclude = []

    if not include:
        include = [mb.name for mb in mailboxes]

    db_cn = db.initialize(database_file) 
    mb_summary = db.summary(db_cn, imap_host)

    log.display('cycling mailboxes...')

    for mb in mailboxes:

        if mb.name in exclude:
            log.display('[{0}]: skip - excluded', mb.name)
            continue

        if mb.name not in include:
            log.display('[{0}]: skip - not included', mb.name)
            continue

        if mb.msg_count == mb_summary.get(mb.name, 0):
            log.display('[{0}]: skip - mailbox messages already recorded', mb.name)
            continue
        
        try:
            server.select_folder(mb.name)
        except IMAP4.error as ex:
            log.display('[{0}]: skip - folder does not exist', mb.name)
            continue

        log.display('[{0}]: start logging emails', mb.name)
        log.display('[{0}]: fetch message uids', mb.name)
        message_ids = server.search()

        log.display('[{0}]: copying meta data...', mb.name)
        recorded_ids = db.message_ids(db_cn, mb.name)
        for msg_ids in chunks(message_ids, 100):
            msg_meta = server.fetch(msg_ids, ('INTERNALDATE', 'FLAGS'))
            for msgid, msg_info in msg_meta.items():
                try:
                    if msgid not in recorded_ids: 
                        db.insert(db_cn, msgid, imap_host, mb.name, mb.uidvalidity, mb.delimiter, msg_info['INTERNALDATE'], ' '.join(msg_info['FLAGS']))
                        log.display('[{0}]:    msg {1} ({2})', mb.name, msgid, msg_info['FLAGS'])
                except db.IntegrityError as ex:
                    log.display('[{0}]:    msg {0} already recorded, but in a different mailbox', mb.name, msgid)
                
        log.display('[{0}]: done.', mb.name)


split_line_regex = re.compile('^(.+) -- \d+ --> ?(.*)$', re.I)


def parse_translation_file(translation_file):
    with open(translation_file, 'r') as fh_:
        for line in fh_.readlines():
            if line.startswith('#') or line.strip() == '':
                continue
            match = split_line_regex.match(line)
            if match:
                grps = match.groups()
                yield grps[0].strip(), grps[1].strip()


response_regex = re.compile('^\[APPENDUID \d+ (\d+)\].*$', re.I)


def parse_append_response(response):
    # [APPENDUID 1436927290 23] Append completed.
    match = response_regex.match(response)
    if not match:
        raise Exception('Invalid append response.')
    return match.groups()[0]


def update_folder_translation(folders, translation_file):
    log.display('fetching folder translations')
    if os.path.exists(translation_file):
        for folder_name, new_folder_name in parse_translation_file(translation_file):
            if folders.has_key(folder_name) and new_folder_name:
                folders[folder_name].migration_name = new_folder_name


def migrate_mail(imap_host, 
                 server_factory, 
                 to_imap_host, 
                 to_server_factory, 
                 database_file, 
                 translation_file, 
                 imap_delay, 
                 batch_delay, 
                 batch_size): 

    log.display('initializing database')
    db_cn = db.initialize(database_file) 

    log.display('fetching source emails')
    mb_folders = db.folder_info(db_cn, imap_host)

    update_folder_translation(mb_folders, translation_file)

    server = server_factory()
    to_server = to_server_factory()
    batch_count = 0

    for mbf in mb_folders.values():
        if mbf.migration_name == '#NO':
            continue

        log.display('[{}] --> [{}]', mbf.name, mbf.migration_name) 
        log.display('[{0}] --> [{1}]: checking migration folder', mbf.name, mbf.migration_name)

        if not to_server.folder_exists(mbf.migration_name):
            log.display('[{0}] --> [{1}]: creating migration folder', mbf.name, mbf.migration_name)
            to_server.create_folder(mbf.migration_name)

        log.display('[{0}] --> [{1}]: selecting source folder', mbf.name, mbf.migration_name)
        server.select_folder(mbf.name)

        log.display('[{0}] --> [{1}]: selecting migration folder', mbf.name, mbf.migration_name)
        to_server.select_folder(mbf.migration_name)

        for msg in db.migration_messages(db_cn, mbf.name): 
            batch_count += 1
            if batch_count == batch_size:
                time.sleep(batch_delay)
                batch_count = 0
            db.update_attempt(db_cn, mbf.name, mbf.uidvalidity, msg.uid) 
            log.display('[{0}] --> [{1}]: processing msg uid {2}', mbf.name, mbf.migration_name, msg.uid)
            result = server.fetch([msg.uid], ['FLAGS', 'RFC822'])
            log.display('[{0}] --> [{1}]:     msg pulled', mbf.name, mbf.migration_name)
            message = result[msg.uid]['RFC822']
            log.display('[{0}] --> [{1}]:     msg migrating', mbf.name, mbf.migration_name)
            response = to_server.append(mbf.migration_name, message, msg.parse_flags(), msg_time=msg.parse_internaldate()) 
            to_uid = parse_append_response(response)

            log.display('[{0}] --> [{1}]:     msg appended as {2}', mbf.name, mbf.migration_name, to_uid)
            db.update(db_cn, to_imap_host, to_uid, mbf.migration_name, mbf.name, mbf.uidvalidity, msg.uid)

            time.sleep(imap_delay)

            
def backup_mail(imap_host, 
                server_factory, 
                database_file, 
                translation_file, 
                local_dir,
                imap_delay, 
                batch_delay, 
                batch_size): 

    log.display('initializing database')
    db_cn = db.initialize(database_file) 

    log.display('fetching source emails')
    mb_folders = db.folder_info(db_cn, imap_host)

    update_folder_translation(mb_folders, translation_file)

    server = server_factory()
    batch_count = 0

    for mbf in mb_folders.values():
        if mbf.migration_name == '#NO':
            continue

        mbf.migration_name = os.path.join(local_dir, util.normalize_folders(mbf.migration_name))

        log.display('[{}] --> [{}]', mbf.name, mbf.migration_name) 
        log.display('[{0}] --> [{1}]: checking local folder', mbf.name, mbf.migration_name)

        if not os.path.exists(mbf.migration_name):
            log.display('[{0}] --> [{1}]: creating local folder', mbf.name, mbf.migration_name)
            os.makedirs(mbf.migration_name)

        log.display('[{0}] --> [{1}]: selecting source folder', mbf.name, mbf.migration_name)
        server.select_folder(mbf.name)

        for msg in db.backup_messages(db_cn, mbf.name): 
            batch_count += 1
            if batch_count == batch_size:
                time.sleep(batch_delay)
                batch_count = 0

            db.update_local_attempt(db_cn, mbf.name, mbf.uidvalidity, msg.uid)

            log.display('[{0}] --> [{1}]: processing msg uid {2}', mbf.name, mbf.migration_name, msg.uid)
            result = server.fetch([msg.uid], ['FLAGS', 'RFC822'])
            log.display('[{0}] --> [{1}]:     msg pulled', mbf.name, mbf.migration_name)
            message = result[msg.uid]['RFC822']
            
            # local save
            uuid = util.uuid_gen()
            util.save_to_local(mbf.migration_name, uuid, message)
            db.update_local(db_cn, uuid, mbf.migration_name, mbf.name, mbf.uidvalidity, msg.uid) 

            log.display('[{0}] --> [{1}]:     msg save locally as {2}', mbf.name, mbf.migration_name, uuid)

            time.sleep(imap_delay)
