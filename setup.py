import os
from setuptools import setup, find_packages 

__version__ = '0.2.6'

readme_file= os.path.join(os.path.dirname(__file__), 'README')
long_description = open(readme_file, 'r').read()

requirements = [
    'IMAPClient==0.13'
    ]

setup(  
    name='imapit',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requirements,
    version=__version__,
    description='imapit',
    author='Mark Gemmill',
    author_email='mark.gemmill@gmail.com',
    license='Simplified BSD License',
    zip_safe=False,
    classifiers=[
        "Natural Language :: English",
        "Programming Language :: Python",
        "License :: OSI Approved :: BSD License",
        "Development Status :: 3 - Alpha",
    ],
    
    ### sample entry point ###
    entry_points={'console_scripts':['imapit = imapit:main'] }
)
